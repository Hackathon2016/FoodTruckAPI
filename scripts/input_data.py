import numpy as np
from random import randint, random
import pprint
from itertools import product, starmap

def main():
	# Variables de control
	matrix_size = (20,20)
	size = 1
	rand_x = 0
	rand_y = 3
	matrix = np.random.randint(rand_x, rand_y, matrix_size)
	matrix_norm = np.zeros(matrix_size)
	# Generando matriz
	for puntos in xrange(randint(1,5)):
		pivot_x = randint(0, matrix_size[0] - 1)
		pivot_y = randint(0, matrix_size[1] - 1)
		for k in xrange(0,randint(4,8)):
			cells = list()
			for j in xrange(pivot_y-k, pivot_y+k):
				for i in xrange(pivot_x-k, pivot_x+k):
					cells.append((i,j))
			#cells = starmap(lambda a,b: (pivot_x+a, pivot_y+b), product((0,-k,+k), (0,-k,+k)))
			# Iterando en los vecinos
			for (x,y) in list(cells)[0:]:
				if x >= matrix_size[0] or y >= matrix_size[1]:
					pass
				elif x < 0 or y < 0:
					pass
				else:
					matrix[x,y] += 1

	max_m = np.max(matrix)
	matrix_norm = (random() * 0.2 - 0.1) + matrix  * 1.0 / max_m
	matrix_norm[matrix_norm <= 0] = 0

	matrix_norm_vecinos = np.zeros(matrix_size)
	for j in xrange(1, matrix_size[0] - 1):
		for i in xrange(1, matrix_size[0] - 1):
			cells = starmap(lambda a,b: (i+a, j+b), product((0,-1,+1), (0,-1,+1)))
			suma = matrix_norm[x, y]
			for (x, y) in list(cells)[1:]:
				suma += matrix_norm[x, y]
				print suma
			matrix_norm_vecinos[i, j] = suma * 1.0 / 9


	matrix_norm = (random() * 0.2 - 0.1) + matrix  * 1.0 / max_m
	matrix_norm[matrix_norm <= 0] = 0

	print "ENTRADA"
	for row in matrix.tolist():
		print ', '.join(str(x) for x in row), ',',

	print "OBJETIVO"
	for row in matrix_norm_vecinos.tolist():
		print ', '.join(str(x) for x in row), ',',



if __name__ == '__main__':
	main()