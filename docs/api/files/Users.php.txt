<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	public function index()
	{
		echo json_encode(array(
			'table' => 'ci_mod_fta_users',
			'total_rows' => $this->user_model->count_all()
		));
	}

	public function get($id)
	{
		echo json_encode($this->user_model->get($id));
	}

	public function get_all()
	{
		echo json_encode($this->user_model->get_all());	
	}

	public function search(){}

	public function update(){}

	public function delete(){}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
