<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Users_uniform extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() 
	{
		$faker = Faker\Factory::create();

		$zones = [
			[0.2, 0.1],
			[0.133333, 0.0666667],
			[0.1, 0.05],
			[0.08, 0.04]
		];

		foreach ($zones as $coordinates) 
		{
			list($lat,$long) = $coordinates;
			
			for ($i=0; $i < 250; $i++) 
			{ 
				$object = array(
					'firstname' => $faker->firstName,
					'lastname' => $faker->lastName,
					'lat' => $faker->randomFloat(NULL, (25.649032-(float)$lat)*100,(25.649032+(float)$lat)*100)/100.0,
					'long' => $faker->randomFloat(NULL, (-100.289867-(float)$long)*100.0,(-100.289867+(float)$long)*100.0)/100.0,
					'token' => md5('foodtruck'),
					'deleted' => $faker->boolean(50),
					'email' => $faker->safeEmail()
				);
				$this->db->insert('ci_mod_fta_users', $object);		
			}				
		}	
	}

	public function down() 
	{
		$this->db->truncate('ci_mod_fta_users');
	}

}

/* End of file 005_users_uniform.php */
/* Location: ./application/seeders/005_users_uniform.php */