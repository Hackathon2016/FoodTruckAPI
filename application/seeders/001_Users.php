<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Users extends CI_Migration 
{

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() 
	{
		$faker = Faker\Factory::create();

		for ($i=0; $i < 1000; $i++) 
		{ 
			$object = array(
				'firstname' => $faker->firstName,
				'lastname' => $faker->lastName,
				'lat' => $faker->randomFloat(NULL, (25.649032-(float)2.0)*100,(25.649032+(float)2.0)*100)/100.0,
				'long' => $faker->randomFloat(NULL, (-100.289867-(float)1.0)*100.0,(-100.289867+(float)1.0)*100.0)/100.0,
				'token' => md5('foodtruck'),
				'deleted' => $faker->boolean(50),
				'email' => $faker->safeEmail()
			);
			$this->db->insert('ci_mod_fta_users', $object);		
		}		
	}

	public function down() 
	{
		$this->db->truncate('ci_mod_fta_users');
	}

}

/* End of file 001_Users.php */
/* Location: ./application/seeders/001_Users.php */