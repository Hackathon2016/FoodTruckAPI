<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_User_locations extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		date_default_timezone_set("America/Monterrey");
		$faker = Faker\Factory::create();

		for ($i=0; $i < 1000; $i++) 
		{ 
			$date = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now');

			$object = array(
				'user_id' => $faker->numberBetween(0,1000),
				'lat' => $faker->randomFloat(NULL, 25.649032-(float)2.0,25.649032+(float)2.0),
				'long' => $faker->randomFloat(NULL, -100.289867-(float)1.0,-100.289867+(float)1.0),
				'timecreated' => $date->getTimestamp(),
				'deleted' => $faker->boolean(50)
			);
			$this->db->insert('ci_mod_fta_user_locations', $object);
		}
	}

	public function down() {
		$this->db->truncate('ci_mod_fta_user_locations');
	}

}

/* End of file 002_User_locations.php */
/* Location: ./application/seeders/002_User_locations.php */