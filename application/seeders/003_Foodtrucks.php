<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Foodtrucks extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() 
	{
		$faker = Faker\Factory::create();

		for ($i=0; $i < 10000 ; $i++) 
		{
			$object = array(
				'name' => $faker->company(),
				'food_type_id' => $faker->numberBetween(1,20),
				'lat' => $faker->randomFloat(NULL, 25.649032-(float)2.0,25.649032+(float)2.0),
				'long' => $faker->randomFloat(NULL, -100.289867-(float)1.0,-100.289867+(float)1.0),
			);
			$this->db->insert('ci_mod_fta_foodtrucks', $object);
		}
	}

	public function down() {
		$this->db->truncate('ci_mod_fta_foodtrucks');
	}

}

/* End of file 003_Foodtrucks.php */
/* Location: ./application/seeders/003_Foodtrucks.php */