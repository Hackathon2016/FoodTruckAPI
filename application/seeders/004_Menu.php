<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Menu extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() 
	{
		$faker = Faker\Factory::create();

		$this->load->model('foodtruck_model');

		foreach ($this->foodtruck_model->get_all() as $key => $row) 
		{
			for ($i=0; $i < 12 ; $i++) 
			{ 
				$object = array(
					'foodtruck_id' => $row['id'],
					'name' => $faker->word,
					'price' => $faker->numberBetween(20,500),
					'type' => $faker->randomElement(array('bebida','comida','ensalada')),
					'deleted' => $faker->boolean(50)

				);
				$this->db->insert('ci_mod_fta_menu', $object);
			}
		}	
	}

	public function down() {
		$this->db->truncate('ci_mod_fta_menu');
	}

}

/* End of file 004_Menu.php */
/* Location: ./application/seeders/004_Menu.php */