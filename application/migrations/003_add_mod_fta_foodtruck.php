<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_mod_fta_foodtruck extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() 
	{
		$this->add_mod_fta_foodtruck_table();
	}

	public function down() 
	{
		$this->dbforge->drop_table('ci_mod_fta_foodtrucks');
	}

	private function add_mod_fta_foodtruck_table()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'null' => FALSE,
				'auto_increment' => TRUE,
				'unsigned' => TRUE,
				'constraint' => 10
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,
			),
			'food_type_id' => array(
				'type' => 'INT',
				'constraint' => 10,
				'null' => TRUE,
			),
			'lat' => array(
				'type' => 'DECIMAL',
				'constraint' => '18,12',
				'null' => TRUE,
			),
			'long' => array(
				'type' => 'DECIMAL',
				'constraint' => '18,12',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('ci_mod_fta_foodtrucks', TRUE);		
	}
}

/* End of file 003_add_mod_fta_foodtruck.php.php */
/* Location: /Users/dsv/Sites/workshop/food-truck-api/application/migrations/003_add_mod_fta_foodtruck.php */