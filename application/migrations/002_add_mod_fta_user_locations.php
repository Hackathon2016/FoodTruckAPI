<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_mod_fta_user_locations extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() 
	{
		$this->add_mod_fta_user_locations_table();
	}

	public function down() 
	{
		$this->dbforge->drop_table('ci_mod_fta_user_locations');
	}

	private function add_mod_fta_user_locations_table()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'null' => FALSE,
				'auto_increment' => TRUE,
				'unsigned' => TRUE,
				'constraint' => 10
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 10,
				'null' => TRUE,
			),
			'lat' => array(
				'type' => 'DECIMAL',
				'constraint' => '18,12',
				'null' => TRUE,
			),
			'long' => array(
				'type' => 'DECIMAL',
				'constraint' => '18,12',
				'null' => TRUE,
			),
			'timecreated' => array(
				'type' => 'INT',
				'constraint' => '15',
				'null' => TRUE
			),
			'deleted' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('ci_mod_fta_user_locations', TRUE);		
	}
}

/* End of file 002_add_mod_fta_user_locations.php.php */
/* Location: /Users/dsv/Sites/workshop/food-truck-api/application/migrations/002_add_mod_fta_user_locations.php */