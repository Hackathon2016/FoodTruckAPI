<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_email_users extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$fields = array(
		    'email' => array(
		    	'type' => 'VARCHAR',
		    	'constraint' => 255,
		    	'null' => TRUE
		    )
		);
		$this->dbforge->add_column('ci_mod_fta_users', $fields);		
	}

	public function down() 
	{
		$this->dbforge->drop_column('ci_mod_fta_users', 'email');	
	}

}

/* End of file 004_add_email_users.php */
/* Location: ./application/migrations/004_add_email_users.php */