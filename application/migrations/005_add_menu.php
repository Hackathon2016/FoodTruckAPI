<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_menu extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() 
	{
		$this->add_menu_table();
	}

	public function down() 
	{
		$this->dbforge->drop_table('ci_mod_fta_menu');
	}

	private function add_menu_table()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'null' => FALSE,
				'auto_increment' => TRUE,
				'unsigned' => TRUE,
				'constraint' => 10
			),
			'foodtruck_id' => array(
				'type' => 'INT',
				'null' => FALSE,
				'constraint' => 10
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,
			),
			'price' => array(
				'type' => 'DECIMAL',
				'constraint' => '18,4',
				'null' => TRUE,
			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,
			),
			'deleted' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('ci_mod_fta_menu', TRUE);		
	}
}

/* End of file 005_add_menu.php.php */
/* Location: /Users/dsv/Sites/workshop/food-truck-api/application/migrations/005_add_menu.php */