<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Foodtrucks extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('foodtruck_model');
	}

	public function index()
	{
		echo json_encode(array(
			'table' => 'ci_mod_fta_foodtrucks',
			'total_rows' => $this->foodtruck_model->count_all()
		));		
	}

	public function get($id)
	{
		echo json_encode($this->foodtruck_model->get($id));
	}

	public function get_all()
	{
		echo json_encode($this->foodtruck_model->get_all());	
	}

	public function search_nearby()
	{
		$ratio = ($this->input->post('radius'))
			? $this->input->post('radius') * 0.621371
			: 5;
		$latitude  = $this->input->post('lat');
		$longitude = $this->input->post('long');

		$response = ($latitude && $longitude)
			? $this->foodtruck_model->nearby($latitude,$longitude,$ratio):
			array();
		while (count($response) < 5)
		{
			$ratio *= 1.2; 
			$response = ($latitude && $longitude)
			? $this->foodtruck_model->nearby($latitude,$longitude,$ratio):
			array();
		}

		echo json_encode(['trucks' => (empty($response))? NULL: $response ]);
	}	

	public function update(){}

	public function delete(){}
}

/* End of file FoodTrucks.php */
/* Location: ./application/controllers/FoodTrucks.php */
