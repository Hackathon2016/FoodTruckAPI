<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
	}

	public function index(){}

	public function get($foodtruck_id = 0)
	{
		echo json_encode($this->menu_model->get_by_foodtruck($foodtruck_id));
	}
}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */