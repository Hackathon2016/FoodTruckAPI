<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{	
		echo json_encode(['api-name' => 'food-truck-api','response' => TRUE]);
	}

}

/* End of file Welcome.php */
/* Location: ./application/controllers/Welcome.php */