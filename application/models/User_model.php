<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function count_all()
	{
		return $this->db->count_all_results('ci_mod_fta_users');
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		$this->db->limit(1);
		$query = $this->db->get('ci_mod_fta_users');
		return $query->row_array();
	}

	public function get_all()
	{
		$query = $this->db->get('ci_mod_fta_users');
		return $query->result_array();
	}

	public function search(array $params, $restricted_mode = FALSE)
	{
		foreach ($params as $key => $value) 
		{
			if (! $restricted_mode) 
			{
				$this->db->or_where($key, $value);
			}
			else
			{
				$this->db->where($key, $value);
			}
		}
		$query = $this->db->get('ci_mod_fta_users');
		return $query->result_array();		
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('ci_mod_fta_users');
	}

	public function update(array $data)
	{
		return $this->db->replace('ci_mod_fta_users', $data);
	}

	public function nearby($lat, $long, $ratio = NULL)
	{
		abs($ratio) <= 0 && $ratio = $this->_ratio;
		$this->db->select('u.firstname, u.lastname, u.lat, u.long');
		$this->db->select(
			'SQRT(POW(69.1 * (u.lat - '.$lat.'),2)'.
			' + POW(69.1 * ('.$long.' - u.long) * COS(u.lat / 57.3), 2)) as "distance"'
		);
		$this->db->from('ci_mod_fta_users u');
		$this->db->having('distance < '.$ratio);
		$this->db->order_by('distance', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}	

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */