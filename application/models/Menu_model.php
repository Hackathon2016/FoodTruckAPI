<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model 
{	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_by_foodtruck($foodtruck_id)
	{
		$this->db->from('ci_mod_fta_menu');
		$this->db->where('foodtruck_id', $foodtruck_id);
		$query = $this->db->get();
		return $query->result_array();
	}	
}

/* End of file Menu_model.php */
/* Location: ./application/models/Menu_model.php */